class PagesController < ApplicationController
  def activity
  end

  def program
  end

  def report
  end

  def event
  end

  def index
  end
end
