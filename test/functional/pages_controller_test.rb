require 'test_helper'

class PagesControllerTest < ActionController::TestCase
  test "should get activity" do
    get :activity
    assert_response :success
  end

  test "should get program" do
    get :program
    assert_response :success
  end

  test "should get report" do
    get :report
    assert_response :success
  end

  test "should get event" do
    get :event
    assert_response :success
  end

end
