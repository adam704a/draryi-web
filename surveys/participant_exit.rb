# encoding: UTF-8
survey "Participant Exit Survey" do

  section "Demographic" do

  	q_name "What is your complete name?"
    a_1 :string
    a_2 :string

    q_nickname "What is your nick name?"
    a_1 :string

    q_dob "What is your date of birth?"
    a_1 :string

	q_addr "What is your address?"
    a_1 :string

 	q_sex_1 "What is your sex?", :pick => :one
    a_1 "Male"
    a_2 "Female"

	q_facebook "What is your Facebook user name?"
    a_1 :string

    q_emergency "Who always knows how to get in touch with you?" 
    a_1 :string


  end

  section "School" do

  	q_in_school "Are you currently in school?", :pick => :one
    a_1 "Yes"
    a_2 "No"

    q_emergency_contact "What is the name of your school?" 
    a_1 :string

    q "What grade are you currently enrolled in", :pick => :one, :display_type => :slider
    (1..10).to_a.each{|num| a num.to_s}

    q_alertajoven "Have you ever completed any technical courses since enrolling in the Alerta Joven Program?", :pick => :one
    a_1 "Yes"
    a_2 "No"

  end


end