# encoding: UTF-8
survey "Participant Registration" do


 section "Registration" do

  group "What is your full name?", :display_type => :inline do
      q "First Name"
      a :string

      q "Last Name"
      a :string

       q "Nickname"
       a :string
    end

    group "What is your home address", :display_type => :inline do
      
      q "Street Address"
      a :string

      q "Neighborhood"
      a :string
    end

     q "What is your birthday?"
     a :date

      question "What is your gender?", :pick => :one
       answer "Male"
       answer "Female"

    group "What is your phone numbers?", :display_type => :inline do
      q "Cell"
      a :string, :input_mask => '(999)999-9999', :input_mask_placeholder => '#'

      q "Home"
      a :string, :input_mask => '(999)999-9999', :input_mask_placeholder => '#'
    end

     q "What is your email address?"
     a :string

      q "What is your Facebook id?"
     a :string

     group "Who is the person who always know how to contact you?", :display_type => :inline do
      
      q "Cell"
      a :string, :input_mask => '(999)999-9999', :input_mask_placeholder => '#'

      q "Cell"
      a :string, :input_mask => '(999)999-9999', :input_mask_placeholder => '#'

      q "Relationship"
      a :string
    end

 end


end